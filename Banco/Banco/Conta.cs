﻿using System;

namespace Banco
{
  public abstract class Conta
  {
    protected double Saldo { get; set; }
    public int Numero { get; set; }
    public string Nome { get; set; }

    protected Conta(double saldo, string nome)
    {
      GerarNumeroConta();

      Saldo = saldo;
      Nome = nome;
    }

    protected abstract void GerarNumeroConta();

    public virtual double ConsultarSaldo()
    {
		return this.Saldo;
    }

    public virtual bool Depositar(double valor)
    {
		if (valor <= 0)
        return false;

		this.Saldo += valor;

		return true;
    }

    public virtual bool Sacar(double valor)
    {
		if (valor <= 0)
        return false;

		else if (valor > this.Saldo)
        return false;

		this.Saldo -= valor;

		return true;
    }
  }
}
