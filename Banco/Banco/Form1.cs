﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
  public partial class Form1 : Form
  {
    private bool isContaCorrente;
    public List<ContaCorrente> contasCorrente;
    public List<ContaPoupanca> contasPoupanca;

    public Form1()
    {
      InitializeComponent();
      contasCorrente = new List<ContaCorrente>();
      contasPoupanca = new List<ContaPoupanca>();
    }

    private void contaCorrenteToolStripMenuItem_Click(object sender, EventArgs e)
    {
      gPropietario.Text = string.Empty;
      gNumConta.Text = string.Empty;
      gSaldo.Text = string.Empty;

      gpbGerenciamento.Visible = true;
      gpbCliente.Visible = false;
      isContaCorrente = true;
      lblConta.Text = "Conta Corrente";
    }

    private void contaPoupançaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      gPropietario.Text = string.Empty;
      gNumConta.Text = string.Empty;
      gSaldo.Text = string.Empty;

      gpbGerenciamento.Visible = true;
      gpbCliente.Visible = false;
      isContaCorrente = false;
      lblConta.Text = "Conta Poupança";
    }

    private void btnSalvar_Click(object sender, EventArgs e)
    {
      string cliente = gPropietario.Text;
      double saldoInicial = Convert.ToDouble(gSaldo.Text);

      if (isContaCorrente)
      {
        ContaCorrente conta = new ContaCorrente(saldoInicial, cliente);
        contasCorrente.Add(conta);
        gNumConta.Text = Convert.ToString(conta.Numero);
      }
      else
      {
        ContaPoupanca conta = new ContaPoupanca(saldoInicial, cliente);
        contasPoupanca.Add(conta);
        gNumConta.Text = Convert.ToString(conta.Numero);
      }
    }

    private void btnCancelar_Click(object sender, EventArgs e)
    {
      gpbGerenciamento.Visible = false;
    }

    private void contaCorrenteToolStripMenuItem1_Click(object sender, EventArgs e)
    {
      isContaCorrente = true;
      gpbCliente.Visible = true;
      gpbGerenciamento.Visible = false;
      clblConta.Text = "Conta Corrente";
      cValor.Text = string.Empty;

      var bindingSource1 = new BindingSource();
      bindingSource1.DataSource = contasCorrente;

      cContas.DataSource = bindingSource1.DataSource;

      cContas.DisplayMember = "Numero";
      cContas.ValueMember = "Numero";
    }

    private void contaPoupancaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      isContaCorrente = false;
      gpbCliente.Visible = true;
      gpbGerenciamento.Visible = false;
      clblConta.Text = "Conta Poupança";
      cValor.Text = string.Empty;

      var bindingSource1 = new BindingSource();
      bindingSource1.DataSource = contasPoupanca;

      cContas.DataSource = bindingSource1.DataSource;

      cContas.DisplayMember = "Numero";
      cContas.ValueMember = "Numero";
    }

    private void btnConsultarSaldo_Click(object sender, EventArgs e)
    {
      double saldo;
      if (isContaCorrente)
      {
        saldo = contasCorrente.FirstOrDefault(x => x.Numero == Convert.ToInt32(cContas.SelectedValue)).ConsultarSaldo();
      }
      else
      {
        saldo = contasPoupanca.FirstOrDefault(x => x.Numero == Convert.ToInt32(cContas.SelectedValue)).ConsultarSaldo();
      }

      MessageBox.Show("Seu Saldo é: " + saldo);
    }

    private void btnDepositar_Click(object sender, EventArgs e)
    {
      Conta conta;

      if (isContaCorrente)
      {
        conta = contasCorrente.FirstOrDefault(x => x.Numero == Convert.ToInt32(cContas.SelectedValue));
      }
      else
      {
        conta = contasPoupanca.FirstOrDefault(x => x.Numero == Convert.ToInt32(cContas.SelectedValue));
      }


      double valor = Convert.ToDouble(cValor.Text);
      if (!(conta.Depositar(valor)))
        MessageBox.Show("Não foi possivel depositar o valor", "Depósito", MessageBoxButtons.OK, MessageBoxIcon.Error);
      else
        MessageBox.Show("Depósito realizado com sucesso!", "Depósito", MessageBoxButtons.OK,MessageBoxIcon.Information);

      cValor.Text = string.Empty;
    }

    private void btnSacar_Click(object sender, EventArgs e)
    {
      Conta conta;

      if (isContaCorrente)
      {
        conta = contasCorrente.FirstOrDefault(x => x.Numero == Convert.ToInt32(cContas.SelectedValue));
      }
      else
      {
        conta = contasPoupanca.FirstOrDefault(x => x.Numero == Convert.ToInt32(cContas.SelectedValue));
      }

      double valor = Convert.ToDouble(cValor.Text);
      if (!(conta.Sacar(valor)))
        MessageBox.Show("Não foi possivel sacar o valor", "Saque", MessageBoxButtons.OK, MessageBoxIcon.Error);
      else
        MessageBox.Show("Saque realizado com sucesso!", "Saque", MessageBoxButtons.OK, MessageBoxIcon.Information);

      cValor.Text = string.Empty;
    }

    private void cContas_SelectedValueChanged(object sender, EventArgs e)
    {
      if (isContaCorrente)
      {
        if (contasCorrente.Count != 0)
        {
          var c = (Conta)cContas.SelectedValue;
          cProprietario.Text = contasCorrente.FirstOrDefault(x => x.Numero == c.Numero).Nome;
        }
      }
      else
      {
        if (contasPoupanca.Count != 0)
        {
          var c = (Conta)cContas.SelectedValue;
          cProprietario.Text = contasPoupanca.FirstOrDefault(x => x.Numero == c.Numero).Nome;
        }
      }
    }
  }
}
