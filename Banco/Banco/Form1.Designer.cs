﻿namespace Banco
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.gerenciamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.criarContaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.contaCorrenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.contaPoupançaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.contaCorrenteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.contaPoupancaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.gpbGerenciamento = new System.Windows.Forms.GroupBox();
      this.lblConta = new System.Windows.Forms.Label();
      this.btnCancelar = new System.Windows.Forms.Button();
      this.btnSalvar = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.gSaldo = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.gPropietario = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.gNumConta = new System.Windows.Forms.TextBox();
      this.gpbCliente = new System.Windows.Forms.GroupBox();
      this.btnSacar = new System.Windows.Forms.Button();
      this.btnDepositar = new System.Windows.Forms.Button();
      this.btnConsultarSaldo = new System.Windows.Forms.Button();
      this.label7 = new System.Windows.Forms.Label();
      this.cContas = new System.Windows.Forms.ComboBox();
      this.cValor = new System.Windows.Forms.TextBox();
      this.clblConta = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.cProprietario = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.menuStrip1.SuspendLayout();
      this.gpbGerenciamento.SuspendLayout();
      this.gpbCliente.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gerenciamentoToolStripMenuItem,
            this.clienteToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(1055, 24);
      this.menuStrip1.TabIndex = 0;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // gerenciamentoToolStripMenuItem
      // 
      this.gerenciamentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.criarContaToolStripMenuItem});
      this.gerenciamentoToolStripMenuItem.Name = "gerenciamentoToolStripMenuItem";
      this.gerenciamentoToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
      this.gerenciamentoToolStripMenuItem.Text = "Gerenciamento";
      // 
      // criarContaToolStripMenuItem
      // 
      this.criarContaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contaCorrenteToolStripMenuItem,
            this.contaPoupançaToolStripMenuItem});
      this.criarContaToolStripMenuItem.Name = "criarContaToolStripMenuItem";
      this.criarContaToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
      this.criarContaToolStripMenuItem.Text = "Criar Conta";
      // 
      // contaCorrenteToolStripMenuItem
      // 
      this.contaCorrenteToolStripMenuItem.Name = "contaCorrenteToolStripMenuItem";
      this.contaCorrenteToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
      this.contaCorrenteToolStripMenuItem.Text = "Conta Corrente";
      this.contaCorrenteToolStripMenuItem.Click += new System.EventHandler(this.contaCorrenteToolStripMenuItem_Click);
      // 
      // contaPoupançaToolStripMenuItem
      // 
      this.contaPoupançaToolStripMenuItem.Name = "contaPoupançaToolStripMenuItem";
      this.contaPoupançaToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
      this.contaPoupançaToolStripMenuItem.Text = "Conta Poupança";
      this.contaPoupançaToolStripMenuItem.Click += new System.EventHandler(this.contaPoupançaToolStripMenuItem_Click);
      // 
      // clienteToolStripMenuItem
      // 
      this.clienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contaCorrenteToolStripMenuItem1,
            this.contaPoupancaToolStripMenuItem});
      this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
      this.clienteToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
      this.clienteToolStripMenuItem.Text = "Cliente";
      // 
      // contaCorrenteToolStripMenuItem1
      // 
      this.contaCorrenteToolStripMenuItem1.Name = "contaCorrenteToolStripMenuItem1";
      this.contaCorrenteToolStripMenuItem1.Size = new System.Drawing.Size(162, 22);
      this.contaCorrenteToolStripMenuItem1.Text = "Conta Corrente";
      this.contaCorrenteToolStripMenuItem1.Click += new System.EventHandler(this.contaCorrenteToolStripMenuItem1_Click);
      // 
      // contaPoupancaToolStripMenuItem
      // 
      this.contaPoupancaToolStripMenuItem.Name = "contaPoupancaToolStripMenuItem";
      this.contaPoupancaToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
      this.contaPoupancaToolStripMenuItem.Text = "Conta Poupanca";
      this.contaPoupancaToolStripMenuItem.Click += new System.EventHandler(this.contaPoupancaToolStripMenuItem_Click);
      // 
      // gpbGerenciamento
      // 
      this.gpbGerenciamento.Controls.Add(this.lblConta);
      this.gpbGerenciamento.Controls.Add(this.btnCancelar);
      this.gpbGerenciamento.Controls.Add(this.btnSalvar);
      this.gpbGerenciamento.Controls.Add(this.label3);
      this.gpbGerenciamento.Controls.Add(this.gSaldo);
      this.gpbGerenciamento.Controls.Add(this.label2);
      this.gpbGerenciamento.Controls.Add(this.gPropietario);
      this.gpbGerenciamento.Controls.Add(this.label1);
      this.gpbGerenciamento.Controls.Add(this.gNumConta);
      this.gpbGerenciamento.Location = new System.Drawing.Point(13, 42);
      this.gpbGerenciamento.Name = "gpbGerenciamento";
      this.gpbGerenciamento.Size = new System.Drawing.Size(493, 229);
      this.gpbGerenciamento.TabIndex = 1;
      this.gpbGerenciamento.TabStop = false;
      this.gpbGerenciamento.Text = "Gerenciamento";
      this.gpbGerenciamento.Visible = false;
      // 
      // lblConta
      // 
      this.lblConta.AutoSize = true;
      this.lblConta.ForeColor = System.Drawing.SystemColors.ControlText;
      this.lblConta.Location = new System.Drawing.Point(199, 28);
      this.lblConta.Name = "lblConta";
      this.lblConta.Size = new System.Drawing.Size(78, 13);
      this.lblConta.TabIndex = 3;
      this.lblConta.Text = "Conta Corrente";
      // 
      // btnCancelar
      // 
      this.btnCancelar.Location = new System.Drawing.Point(368, 181);
      this.btnCancelar.Name = "btnCancelar";
      this.btnCancelar.Size = new System.Drawing.Size(75, 23);
      this.btnCancelar.TabIndex = 4;
      this.btnCancelar.Text = "&Cancelar";
      this.btnCancelar.UseVisualStyleBackColor = true;
      this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
      // 
      // btnSalvar
      // 
      this.btnSalvar.Location = new System.Drawing.Point(286, 181);
      this.btnSalvar.Name = "btnSalvar";
      this.btnSalvar.Size = new System.Drawing.Size(75, 23);
      this.btnSalvar.TabIndex = 3;
      this.btnSalvar.Text = "&Salvar";
      this.btnSalvar.UseVisualStyleBackColor = true;
      this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(25, 142);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(67, 13);
      this.label3.TabIndex = 1;
      this.label3.Text = "Saldo Inicial:";
      // 
      // gSaldo
      // 
      this.gSaldo.Location = new System.Drawing.Point(130, 139);
      this.gSaldo.Name = "gSaldo";
      this.gSaldo.Size = new System.Drawing.Size(100, 20);
      this.gSaldo.TabIndex = 2;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(25, 108);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(60, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Propietário:";
      // 
      // gPropietario
      // 
      this.gPropietario.Location = new System.Drawing.Point(130, 105);
      this.gPropietario.Name = "gPropietario";
      this.gPropietario.Size = new System.Drawing.Size(313, 20);
      this.gPropietario.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(25, 73);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(93, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Numero da Conta:";
      // 
      // gNumConta
      // 
      this.gNumConta.Enabled = false;
      this.gNumConta.Location = new System.Drawing.Point(130, 70);
      this.gNumConta.Name = "gNumConta";
      this.gNumConta.Size = new System.Drawing.Size(100, 20);
      this.gNumConta.TabIndex = 0;
      // 
      // gpbCliente
      // 
      this.gpbCliente.Controls.Add(this.btnSacar);
      this.gpbCliente.Controls.Add(this.btnDepositar);
      this.gpbCliente.Controls.Add(this.btnConsultarSaldo);
      this.gpbCliente.Controls.Add(this.label7);
      this.gpbCliente.Controls.Add(this.cContas);
      this.gpbCliente.Controls.Add(this.cValor);
      this.gpbCliente.Controls.Add(this.clblConta);
      this.gpbCliente.Controls.Add(this.label5);
      this.gpbCliente.Controls.Add(this.cProprietario);
      this.gpbCliente.Controls.Add(this.label6);
      this.gpbCliente.Location = new System.Drawing.Point(543, 42);
      this.gpbCliente.Name = "gpbCliente";
      this.gpbCliente.Size = new System.Drawing.Size(493, 229);
      this.gpbCliente.TabIndex = 5;
      this.gpbCliente.TabStop = false;
      this.gpbCliente.Text = "Cliente";
      this.gpbCliente.Visible = false;
      // 
      // btnSacar
      // 
      this.btnSacar.Location = new System.Drawing.Point(327, 151);
      this.btnSacar.Name = "btnSacar";
      this.btnSacar.Size = new System.Drawing.Size(95, 23);
      this.btnSacar.TabIndex = 5;
      this.btnSacar.Text = "Sacar Valor";
      this.btnSacar.UseVisualStyleBackColor = true;
      this.btnSacar.Click += new System.EventHandler(this.btnSacar_Click);
      // 
      // btnDepositar
      // 
      this.btnDepositar.Location = new System.Drawing.Point(212, 151);
      this.btnDepositar.Name = "btnDepositar";
      this.btnDepositar.Size = new System.Drawing.Size(106, 23);
      this.btnDepositar.TabIndex = 5;
      this.btnDepositar.Text = "Depositar Valor";
      this.btnDepositar.UseVisualStyleBackColor = true;
      this.btnDepositar.Click += new System.EventHandler(this.btnDepositar_Click);
      // 
      // btnConsultarSaldo
      // 
      this.btnConsultarSaldo.Location = new System.Drawing.Point(110, 151);
      this.btnConsultarSaldo.Name = "btnConsultarSaldo";
      this.btnConsultarSaldo.Size = new System.Drawing.Size(95, 23);
      this.btnConsultarSaldo.TabIndex = 5;
      this.btnConsultarSaldo.Text = "Consultar Saldo";
      this.btnConsultarSaldo.UseVisualStyleBackColor = true;
      this.btnConsultarSaldo.Click += new System.EventHandler(this.btnConsultarSaldo_Click);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(284, 196);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(34, 13);
      this.label7.TabIndex = 1;
      this.label7.Text = "Valor:";
      // 
      // cContas
      // 
      this.cContas.FormattingEnabled = true;
      this.cContas.Location = new System.Drawing.Point(164, 69);
      this.cContas.Name = "cContas";
      this.cContas.Size = new System.Drawing.Size(113, 21);
      this.cContas.TabIndex = 4;
      this.cContas.SelectedValueChanged += new System.EventHandler(this.cContas_SelectedValueChanged);
      // 
      // cValor
      // 
      this.cValor.Location = new System.Drawing.Point(322, 193);
      this.cValor.Name = "cValor";
      this.cValor.Size = new System.Drawing.Size(100, 20);
      this.cValor.TabIndex = 2;
      // 
      // clblConta
      // 
      this.clblConta.AutoSize = true;
      this.clblConta.ForeColor = System.Drawing.SystemColors.ControlText;
      this.clblConta.Location = new System.Drawing.Point(199, 28);
      this.clblConta.Name = "clblConta";
      this.clblConta.Size = new System.Drawing.Size(78, 13);
      this.clblConta.TabIndex = 3;
      this.clblConta.Text = "Conta Corrente";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(31, 73);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(107, 13);
      this.label5.TabIndex = 1;
      this.label5.Text = "Selecione sua conta:";
      // 
      // cProprietario
      // 
      this.cProprietario.Location = new System.Drawing.Point(164, 108);
      this.cProprietario.Name = "cProprietario";
      this.cProprietario.Size = new System.Drawing.Size(313, 20);
      this.cProprietario.TabIndex = 1;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(59, 111);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(60, 13);
      this.label6.TabIndex = 1;
      this.label6.Text = "Propietário:";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1055, 306);
      this.Controls.Add(this.gpbCliente);
      this.Controls.Add(this.gpbGerenciamento);
      this.Controls.Add(this.menuStrip1);
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "Form1";
      this.Text = "Banco do Cotemig";
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.gpbGerenciamento.ResumeLayout(false);
      this.gpbGerenciamento.PerformLayout();
      this.gpbCliente.ResumeLayout(false);
      this.gpbCliente.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem gerenciamentoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem criarContaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem contaCorrenteToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem contaPoupançaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
    private System.Windows.Forms.GroupBox gpbGerenciamento;
    private System.Windows.Forms.Label lblConta;
    private System.Windows.Forms.Button btnCancelar;
    private System.Windows.Forms.Button btnSalvar;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox gSaldo;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox gPropietario;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox gNumConta;
    private System.Windows.Forms.ToolStripMenuItem contaCorrenteToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem contaPoupancaToolStripMenuItem;
    private System.Windows.Forms.GroupBox gpbCliente;
    private System.Windows.Forms.Label clblConta;
    private System.Windows.Forms.ComboBox cContas;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox cProprietario;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Button btnSacar;
    private System.Windows.Forms.Button btnDepositar;
    private System.Windows.Forms.Button btnConsultarSaldo;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox cValor;
  }
}

