﻿using System;

namespace Banco
{
  public class ContaCorrente : Conta
  {
    public ContaCorrente(double saldo, string nome) : base(saldo, nome)
    {
    }

    protected override void GerarNumeroConta()
    {
      Random rnd = new Random();
      this.Numero = rnd.Next(20000, 29999);
    }
  }
}
