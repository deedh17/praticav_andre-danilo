﻿using System;

namespace Banco
{
  public class ContaPoupanca : Conta
  {
    public ContaPoupanca(double saldo, string nome) : base(saldo, nome)
    {
    }

    protected override void GerarNumeroConta()
    {
      Random rnd = new Random();
      this.Numero = rnd.Next(10000, 19999);
    }
  }
}
